#!/usr/bin/env python3

"""
Abuse Parser (ap.py) is a tool for managing an abuse@ email address.

It downloads email from an IMAP email box, stores it in a sqlite database,
and tries to analyze it, creating categories and sub-categories. It also
forwards email it thinks are not abuse reports and can send summaries
of the emails that are abuse reports.
"""

import sys
import os
import re
import pydoc
import datetime
import argparse
import socket
from email.utils import parsedate_to_datetime
from email.mime.text import MIMEText
from email.header import Header
import smtplib
import sqlite3
import imaplib
import pytz

class AbuseReportParser:
    """
    Class for managing automated email sent to abuse addresses.
    """
    # Configuration.
    conf = {}
    # Command line arguments
    args = None
    # Compiled regular expressions for matching lines in the email that
    # will identify the sender, date and service of the email.
    compiled_regexs = {'date': None, 'sender': {}, 'service': {}}
    # When parsing email messages, whether or not we have definitely found
    # the sender.
    found_sender = False
    # The lines of the report when printing a summary
    report = []
    # The sqlite database
    db = None #pylint: disable=invalid-name
    # THe sqlite db cursor
    db_cursor = None
    PRIORITY_INFO = 'info'
    PRIORITY_ERROR = 'error'

    def log(self, msg, priority=None):
        """
        Print errors depending on log level.

        Parameters
        str (msg): message to print
        str (priority): log level
        """
        if priority is None:
            priority = self.PRIORITY_INFO
        if priority == self.PRIORITY_INFO and not self.args.quiet:
            print(msg)
        if priority == self.PRIORITY_ERROR:
            print(msg)

    def init(self):
        """
        Start the whole process.
        """
        self.parse_args()
        # Assume the worst.
        exit_code = 1
        if self.parse_config():
            self.init_db()
            if self.args.func():
                exit_code = 0
            self.commit_db()
        sys.exit(exit_code)

    def parse_args(self) -> None:
        """
        Parse arguments.
        """
        parser = argparse.ArgumentParser(
            description='Download, parse and analyze email sent an abuse@ email address.'
        )
        parser.add_argument(
            '--config',
            help='Path to config file. ~/.config/abuse-report-parser.conf by default.'
        )
        parser.add_argument(
            '--quiet',
            help='Only print errors.',
            action='store_true'
        )
        parser.set_defaults(config='', quiet=False)
        subparsers = parser.add_subparsers()
        subparsers.required = True

        parser_download = subparsers.add_parser(
            'download', help='Fetch mail into the database from the mailbox via IMAP'
        )
        parser_download.set_defaults(func=self.download)

        parser_analyze = subparsers.add_parser(
            'analyze',
            help='Analyze email in the database, add cateogiies and other metadata.'
        )
        parser_analyze.add_argument(
            '--rowid',
            help='Optionally pass the rowid to only analyze one message.',
            type=int
        )
        parser_analyze.set_defaults(func=self.analyze)

        parser_delete = subparsers.add_parser(
            'delete',
            help="Delete a message form the database if it is spam or otherwise not relevant)"
        )
        parser_delete.add_argument(
            '--rowid',
            help='Pass the rowid of the message to delete.',
            required=True,
            type=int
        )
        parser_delete.set_defaults(func=self.delete)

        parser_search = subparsers.add_parser(
            'search',
            help='Search records in the database. No arguments dumps all records.'
        )
        parser_search.add_argument(
            '--rowid',
            help='Optionally pass the rowid to only print one message.',
            type=int
        )
        parser_search.add_argument(
            '--sender',
            help=("Optionally search by sender."),
            type=str
        )
        parser_search.add_argument(
            '--service',
            help=("Optionally search by service sending report."),
            type=str
        )
        parser_search.set_defaults(func=self.search)

        parser_summarize = subparsers.add_parser(
            'summarize',
            help='Print a summary report of emails received in the previous period'
        )
        parser_summarize.add_argument(
            '--days',
            help='Specify how many days back, default is 7.',
            type=int
        )
        parser_summarize.add_argument(
            '--email',
            help='Specify email to send report (this option suppresses report output on terminal).',
            type=str
        )
        parser_summarize.set_defaults(func=self.summarize, days=7)

        parser_view = subparsers.add_parser(
            'view',
            help='View the complete message in the database.'
        )
        parser_view.add_argument(
            '--rowid',
            help='Pass the rowid of the message to view.',
            required=True,
            type=int
        )
        parser_view.set_defaults(func=self.view)

        parser_forward = subparsers.add_parser(
            'forward',
            help='Forward messages that do not seem to be abuse reports for human review.'
        )
        parser_forward.add_argument(
            '--email',
            help='The email to send reports.',
            type=str
        )
        parser_forward.add_argument(
            '--rowid',
            help='Optionally only forward the given message, not all messages with no service.',
            type=int
        )
        parser_forward.set_defaults(func=self.forward)

        self.args = parser.parse_args()

    def parse_config(self) -> bool:
        """
        Parse the config files.

        Returns:
        dict: a dictionary of config values.
        """
        if not self.args.config:
            config_dir = os.path.expanduser('~/.config/abuse-report-parser')
            path = f"{config_dir}/arp_config.py"
        else:
            config_dir = os.path.dirname(self.args.config)
            path = self.args.config
        if not os.path.exists(path):
            self.log(f"Failed to find config path, looking in: {path}.", self.PRIORITY_ERROR)
            self.log("Perhaps copy arp_config.py.sample to that locaiton?")
            return False

        sys.path.append(config_dir)
        import arp_config #pylint: disable=import-outside-toplevel,import-error
        self.conf = arp_config.config
        return True

    def init_db(self) -> None:
        """
        Initialize sqlite db.
        """
        self.db = sqlite3.connect(self.conf["database"]) #pylint: disable=invalid-name
        self.db_cursor = self.db.cursor()
        self.db_cursor.execute("""
            CREATE TABLE IF NOT EXISTS email (
                message TEXT NOT NULL DEFAULT '',
                header_utc_datetime TEXT NOT NULL DEFAULT '',
                sender TEXT NOT NULL DEFAULT '',
                service TEXT NOT NULL DEFAULT '',
                notify_email TEXT NOT NULL DEFAULT '',
                notify_date TEXT NOT NULL DEFAULT '',
                analyze_date TEXT NOT NULL DEFAULT ''
            );
        """)

    def commit_db(self) -> bool:
        """
        Ensure db changes are saved.
        """
        return self.db.commit()

    def download(self) -> True:
        """
        Download email and save in sqlite database.
        """

        username = self.conf['username']
        password = self.conf['password']

        if self.conf['imap_encryption_method'] == 'starttls':
            imap = imaplib.IMAP4(self.conf['imap_host'], self.conf['imap_port'])
            imap.starttls()
        elif self.conf['imap_encryption_method'] == 'tls':
            imap = imaplib.IMAP4_SSL(self.conf['imap_host'], self.conf['imap_port'])
        else:
            self.log(
                f"Unknown encryption method: {self.conf['imap_encryption_method']}.",
                self.PRIORITY_ERROR
            )
            return False

        try:
            imap.login(username, password)
        except imaplib.IMAP4.error as err:
            self.log(f"IMAP login failed: {err}", self.PRIORITY_ERROR)
            return False

        imap.select()
        _, messages = imap.search(None, 'ALL')
        count = 0
        if messages:
            messages = messages[0].split(b' ')
            for mail in messages:
                if mail:
                    count += 1
                    _, message = imap.fetch(mail, '(RFC822)')
                    sql = "INSERT INTO email (message) VALUES(?)"
                    params = (message[0][1],)
                    self.db_cursor.execute(sql, params)
                    imap.store(mail, "+FLAGS", "\\Deleted")
        imap.expunge()
        imap.close()
        imap.logout()
        self.log(f"Downloaded {count} message(s).")
        return True

    def analyze(self) -> bool:
        """
        Analyze email in sqlite database, adding meta data.
        """

        # Regexp for extracting the first date header
        self.compiled_regexs['date'] = re.compile(r'^Date:(.*)$')

        # Regxp to figure out what service reported it.
        self.compiled_regexs['service']['generic'] = re.compile(r'This is a (.+) Abuse Report')
        # Hotmail doesn't conform to the standard.
        self.compiled_regexs['service']['hotmail'] = re.compile(r"From: <staff@hotmail.com>")
        # These people don't know how to follow a standard.
        abusix_text = r'Here is an Abuse Report from the Abusix Global Reporting service'
        self.compiled_regexs['service']['abusix'] = re.compile(abusix_text)
        # More "almost there" abuse reporting.
        spamcop_text = r'To change ARF message format to SpamCop format change settings on your preferences page'
        self.compiled_regexs['service']['spamcop'] = re.compile(spamcop_text)

        # Now we have to compile all of our custom regular expressions.
        for key in self.conf["senders"]:
            self.compiled_regexs['sender'][key] = re.compile(self.conf["senders"][key]["regexp"])

        sql = "SELECT rowid, message FROM email WHERE "
        if self.args.rowid:
            sql += "rowid = ?"
            params = (self.args.rowid,)
        else:
            sql += "analyze_date = ''"
            params = ()
        self.db_cursor.execute(sql, params)
        rows = self.db_cursor.fetchall()
        if not rows:
            self.log("No email messages to analyze.")
            return True
        count = 0
        for record in rows:
            count += 1
            self.analyze_record(record)
        self.log(f"Analyzed {count} message(s).")
        return True

    def analyze_record(self, record) -> None:
        """
        Analyze the given record to find service, sender, etc.

        Parameters:
        sqlite record (record): the record returned by the sqlite database.
        """
        rowid = record[0]
        message = record[1]
        service = ''
        sender = ''
        notify_email = ''
        self.found_sender = False
        date_header = None
        for line in message.splitlines():
            line = line.decode()
            # Match the date header.
            if date_header is None:
                date_match = self.compiled_regexs['date'].search(line)
                if date_match:
                    # Convert to a UTC time in the ISO format string.
                    date_header = (parsedate_to_datetime(date_match.group(1))
                            .astimezone(pytz.timezone('UTC'))
                            .strftime('%Y-%m-%d %H:%M:%S'))
                    continue

            # Match the service.
            if not service:
                for service_key in self.compiled_regexs['service']:
                    service_match = self.compiled_regexs['service'][service_key].search(line)
                    if service_match:
                        if service_key == 'generic':
                            service = service_match.group(1)
                            continue
                        else:
                            service = service_key
                        continue

            # And match the sender and notify_email
            if self.found_sender is False:
                sender, notify_email = self.find_sender(line, sender, notify_email)

            if (self.found_sender and service and date_header):
                # We have everything we need.
                break

        if date_header is None:
            self.log("Failed to find date header for {rowid}", self.PRIORITY_ERROR)
            # Something is probably seriously wrong. What should we do??
            return

        if service == '':
            # If we can't positively identify the service, it is most likely not
            # an ARF formatted message and, regarldess of the sender, should be
            # escalated.
            notify_email = self.conf["escalate"]

        if notify_email is None:
            notify_email = ''

        analyze_date = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        sql = """UPDATE email SET
            sender = ?,
            service = ?,
            notify_email = ?,
            header_utc_datetime = ?,
            analyze_date = ?
            WHERE rowid = ?"""
        params = (sender, service, notify_email, date_header, analyze_date, rowid)
        self.db_cursor.execute(sql, params)
        self.log(f"{rowid}: {date_header}: [{service}] {sender}")

    def find_sender(self, line: str, sender: str, notify_email: str) -> tuple:
        """
        Given a line from the email, check against all regular expressions to
        find a match.

        Parameters
        str (line): the line from the email message
        str (sender): the value of the sender variable, if we don't find a match,
        return the existing value.
        str (notify_email): the value of the notify email, if we dont' find a match
        return the existing value.

        Return
        tuple (sender, notify_email): the sender and notify_email
        """
        for regexp_item in self.compiled_regexs['sender'].items():
            key = regexp_item[0]
            regexp = regexp_item[1]
            match_result = regexp.search(line)
            if match_result:
                # Reset sender so we can build a new one.
                sender = ''
                for sender_part in self.conf["senders"][key]["sender"]:
                    if isinstance(sender_part, int):
                        sender += match_result.group(sender_part)
                    else:
                        sender += sender_part
                self.found_sender = self.conf["senders"][key]["final"]
                notify_email = self.conf["senders"][key]["notify"]
                break
        return (sender, notify_email)

    def delete(self) -> bool:
        """
        Delete email from the sqlite database.
        """
        return self.db_cursor.execute("DELETE FROM email WHERE rowid = ?", (self.args.rowid,))

    def search(self) -> bool:
        """
        Search email from sqlite database to standard out.
        """

        rowid = self.args.rowid
        sender = self.args.sender
        service = self.args.service
        sql = ("SELECT rowid, header_utc_datetime, sender, service, notify_email, notify_date "
          "FROM email")
        params = []
        where_clauses = []
        if sender:
            where_clauses.append("sender = ?")
            params.append(sender)
        if rowid:
            where_clauses.append("rowid = ?")
            params.append(rowid)
        if service:
            where_clauses.append("service = ?")
            params.append(service)
        if len(params) > 0:
            sql += " WHERE " + " AND ".join(where_clauses)
        params = tuple(params)
        sql += " ORDER BY header_utc_datetime DESC"
        self.db_cursor.execute(sql, params)
        records = self.db_cursor.fetchall()
        output_generator = self.generate_search_results(records)
        output_text = '\n'.join(output_generator)
        pydoc.pager(output_text)
        print()
        return True

    def generate_search_results(self, records):
        """
        Generator for yielding records found.

        Parameters
        sqlite result (records): result from sqlite query.
        """
        for record in records:
            rowid = record[0]
            header_utc_datetime = record[1]
            sender = record[2]
            service = record[3]
            notify_email = record[4]
            notify_date = record[5]
            notify_line = ""
            if notify_email:
                notify_line = f" ({notify_email}: {notify_date})"
            yield f"{rowid}: {header_utc_datetime}: [{service}] {sender} {notify_line}"

    def summarize(self) -> True:
        """
        Summarize email from sqlite database.
        """

        # Create two date periods based on the "days" variable given by the
        # user. Using the default of 7 days, the first period is from days
        # ago to today and the second period is from 14 days ago until 7 days
        # ago.
        days = self.args.days
        today = datetime.datetime.utcnow()
        first_period_start = today - datetime.timedelta(days=days)
        second_period_start = first_period_start - datetime.timedelta(days=days)
        date_ranges = [
            (first_period_start, today),
            (second_period_start, first_period_start)
        ]
        for date_range in date_ranges:
            first_date = date_range[0].strftime("%Y-%m-%d")
            second_date = date_range[1].strftime("%Y-%m-%d")
            for field in [ "sender", "service" ]:
                self.append_to_report(field, first_date, second_date)

        report = "\n".join(self.report)
        if self.args.email:
            msg = MIMEText(report, 'plain', 'utf-8')
            msg['Subject'] = Header("May First Abuse report", 'utf-8')
            msg['From'] = self.conf["from"]
            msg['To'] = self.args.email

            self.send_message(self.conf["from"], self.args.email, msg.as_string())
        else:
            print(report)
        return True

    def append_to_report(self, field, first_date, second_date):
        """
        Append lines to the report based on query parameters given.
        """
        sql = f"""SELECT {field}, COUNT(*) AS count
            FROM email
            WHERE
                header_utc_datetime > ? AND
                header_utc_datetime < ? AND
                {field} != ''
            GROUP BY {field}
            ORDER BY count DESC LIMIT 10"""
        params = (
            first_date,
            second_date
        )
        self.db_cursor.execute(sql, params)
        self.report.append(f"# Top {field}s between {first_date} and {second_date}")
        for record in self.db_cursor.fetchall():
            field_value = record[0]
            count = record[1]
            self.report.append(f" {count}: {field_value}")
        self.report.append("")

    def view(self) -> True:
        """
        View email from sqlite database.
        """

        self.db_cursor.execute("SELECT message FROM email WHERE rowid = ?", (self.args.rowid,))
        record = self.db_cursor.fetchone()
        pydoc.pager(record[0].decode())
        return True

    def forward(self) -> bool:
        """
        Forward - forward messages with a notify_email value but no notify_date value
        or the message with the given rowid to the email address passed in via --email.
        """

        # Avoid surprises.
        if not self.args.rowid and self.args.email:
            self.log("You specified an email address but you did not specify a rowid "
              "of a record to foward. If you do not specify a rowid, messages will "
              "be forwarded to the notify_email value in the database, not to the "
              "email address you specify as an argument.", self.PRIORITY_ERROR)
            return False
        sql = "SELECT rowid, message, notify_email FROM email WHERE "
        if self.args.rowid:
            sql += "rowid = ?"
            params = (self.args.rowid,)
        else:
            sql += "notify_email != '' AND notify_date = ''"
            params = ()
        self.db_cursor.execute(sql, params)
        return_status = True
        count_success = 0
        count_fail = 0
        for record in self.db_cursor.fetchall():
            rowid = record[0]
            message = record[1].decode()
            if self.args.email:
                # Send to the email specified on the command line.
                email = self.args.email
            else:
                email = record[2]
                if not email:
                    self.log(f"Failed to find an email address for row {rowid}",
                        self.PRIORITY_ERROR)
                    return_status = False
                    # We will keep going, but will return False to generate a non-zero
                    # exit code.
                    continue

            try:
                message.encode('ascii')
            except UnicodeEncodeError:
                # I don't understand why this is happening.
                self.log(f"Stripping non-ascii characters from message {rowid}.")
                message = ''.join(char for char in message if ord(char) < 128)
            if self.send_message(self.conf["from"], email, message):
                count_success += 1
                sql = "UPDATE email SET notify_email = ?, notify_date = ? WHERE rowid = ?"
                params = (
                    email,
                    datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"),
                    rowid
                )
                self.db_cursor.execute(sql, params)
            else:
                count_fail += 1
                return_status = False
        self.log(f"Forwarded {count_success} messages, {count_fail} failed.")
        return return_status

    def send_message(self, from_address, to_address, msg) -> bool:
        """
        Send the given message.

        Parameters:
        str (from_address): address to send message from
        str (to_addresss): address to send message to
        str (msg): the message itself.
        """
        try:
            if self.conf['smtp_encryption_method'] == 'starttls':
                smtp = smtplib.SMTP(self.conf['smtp_host'], self.conf['smtp_port'])
                smtp.ehlo()
                smtp.starttls()
            elif self.conf['smtp_encryption_method'] == 'tls':
                smtp = smtplib.SMTP_SSL(self.conf['smtp_host'], self.conf['smtp_port'])
                smtp.ehlo()
            else:
                self.log(
                    f"Unknown encryption method: {self.conf['smtp_encryption_method']}.",
                    self.PRIORITY_ERROR
                )
                return False
            smtp.login(self.conf['username'], self.conf['password'])
            smtp.sendmail(from_address, to_address, msg)
            smtp.quit()
        except socket.gaierror:
            self.log("ERROR: Email not sent."
              "Unable to connect to remote mail server. Please check the 'host' setting "
              "in your config file.", self.PRIORITY_ERROR)
            return False
        except smtplib.SMTPAuthenticationError:
            self.log("ERROR: Email not sent. Authentication error. Please check your "
              "username and password settings in your config file.", self.PRIORITY_ERROR)
        return True


if __name__ == "__main__":
    arp = AbuseReportParser()
    arp.init()
