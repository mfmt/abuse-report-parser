# Abuse Report Parser

The `abuse_report_parser.py` script, helps manage an email box that receives
abuse reports.

It helps classify email received, escalate messages to a human, and provide
rough reporting to help track trends or spikes.

It is intended to work via timers:

 * An **hourly** timer:
   * *downloads* email from the abuse IMAP box into a sqlite database
   * *analyzes* the email using a configuration file with custom regular
     expresions, attempting to record a "sender" responsible for each email, an
     optional email to forward the report, and the service that sent the abuse
     report. Any email that does not seem like an abuse report is assigned an
     "escalate" email address so it can be forwarded to the appropriate person.
   * *forwards* all email to the appropriate people
 * A **weekly** timer:
   * sends a summary of the past week, showing the top senders responsible for
     abuse reports with a comparison to the previous week

It provides the following subcommands:

 * `search [--source SOURCE] [--sender SENDER] [--rowid ROWID]`: search all
   messages received. Without arguments, show a summary of all messages from
   most recent to least recent, including rowid, a unique identifier for each
   message. Specify `--source` to filter by the service sent the abuse report
   or `--sender` to filter by the sender (as determined by the `analyze`
   sub-command - see below).

 * `view [--rowid ROWID]`: View the message for a given `--rowid`.

 * `forward [--email EMAIL] [--rowid ROWID]`: Forward the abuse report. With no
   arguments, find all messages that have not been forwarded, but have been
   assigned an email address by the `analyze` sub-command (see below).
   Optionally, pass a `--rowid` to only forward a given message or a `--email`
   to override the assigned email and send to the specified email.

 * `summarize [--email EMAIL] [--days DAYS]`: Print (or if `--email` is
   specified, email) a summary of abuse emails over the last 7 days (or
   whatever is passed via `--days`).
    
 * `download`: Download email from an IMAP box into the sqlite database.

 * `analyze [--rowid ROWID]`: Analyze all email in the sqlite database that has
   not been analyzed (or re-analyze a message for a given `--rowid`). The
   `analyze` sub-command reads a list of regular expressions in the config
   file. If a line matches a regular expression, the message can be assigned
   both a "sender" that represents who is responsible for sending the message,
   as well as a notification email for the sender that can be forwarded a copy
   of the email. Additionally, a "escalate" email address can be provided in
   the configuration file for any message that does not appear to be a standard
   abuse report formatted message.

